jQuery( document ).ready( function ( e ) {
	console.log('ready');
	jQuery.ajax({
		type: "GET",
		url: "wp-content/plugins/moxie-wp-test/json/movies.json?callback=JSON_CALLBACK",
		dataType: 'json',
		success: function ( movies ) {
		
			// helper function for setting attr
			function setAttributes(el, attrs) {
			  for(var key in attrs) {
				el.setAttribute(key, attrs[key]);
			  }
			}
			
			var mainElement = document.getElementById("main");
			
			
	/* 		var searchElement = document.createElement("input");
			
			setAttributes(searchElement, {
				"id": "searchListings",
				"placeholder": "Search by name"
			});
			
			mainElement.insertBefore(searchElement, mainElement.childNodes[0]);		 */
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			var containerElement = document.createElement("ul");
			
			setAttributes(containerElement, {
				"id": "movieListing",
				"class": "movies"
			});
			
			mainElement.insertBefore(containerElement, mainElement.childNodes[1]);
			
			var movieListing = document.getElementById("movieListing");
			
			for ( var i = 0; i < movies.length; ++i ) {

			
				var a = document.createElement("li");

				setAttributes(a, {
					"id": "movie" + movies[i]['id'],
					"class": "movie",
					"data-title": movies[i]['title'] 
				});

				var b = document.createElement("h1");
					var title = document.createTextNode(movies[i]['title']);
						b.appendChild(title);
				
				var c = document.createElement("p");
					var year = document.createTextNode("Year: " + movies[i]['year']);
						c.appendChild(year);
						
				var d = document.createElement("p");
					var year = document.createTextNode("Rating: " + movies[i]['rating']);
						d.appendChild(year);
						
				var e = document.createElement("p");
					var short_description = document.createTextNode(movies[i]['short_description']);
						e.appendChild(short_description);
						
				
				
				a.appendChild(b);
				a.appendChild(c);
				a.appendChild(d);
				a.appendChild(e);
				
				//console.log(movies[i]['poster_url']);
				
				if ( movies[i]['poster_url'] ) {
					var f = document.createElement("img");
					setAttributes(f, {
						"src": movies[i]['poster_url']
					});
					a.appendChild(f);
				}
				
				containerElement.appendChild(a);
				
			}
			
				/* var title = movies[i]['title'];
				var id = movies[i]['id'];
				var rating = movies[i]['rating'];
				var year = movies[i]['year'];
				var short_description = movies[i]['short_description']; */
		
		},
		error: function(request, status, error) {
			console.log(request.responseText);
		}



		
	});

	
});