<?php

if ( ! defined( 'ABSPATH' ) ) exit;


// make sure to flush rewrite
function my_rewrite_flush() {

    my_cpt_init();
    flush_rewrite_rules();
}

// after activation
register_activation_hook( __FILE__, 'my_rewrite_flush' );

// after theme is switched
add_action( 'after_switch_theme', 'my_rewrite_flush' );






/*
if ( is_home() || is_front_page() ) {
	echo 'sdjkfh';
}
*/
