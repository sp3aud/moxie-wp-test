<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Custom_Post_Type_Class {
	
	public function create_cpt_movies() {

		// set up the arguments for the movie custom post type
		$args = array(
			'public' => true,
			'show_ui' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'query_var' => true,
			'rewrite' => array(
				'slug' => 'movie',
				'with_front' => false
			),
			'menu_position' => 5,
			'supports' => array(
				'title',
				'thumbnail'
			),
			'taxonomies' => array( '' ),
			'menu_icon' => plugins_url( 'moxie-wp-test/assets/img/moxie-logo.png' ),
			'has_archive' => true,
			'labels' => array(
				'name' => 'Movies',
				'singular_name' => 'Movie',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Movie',
				'edit' => 'Edit',
				'edit_item' => 'Edit Movie',
				'new_item' => 'New Movie',
				'view' => 'View',
				'view_item' => 'View Movie',
				'search_items' => 'Search Movies',
				'not_found' => 'No Movies found',
				'not_found_in_trash' => 'No Movies found in Trash'
			), 

		);	

		// register the movie custom post type
		register_post_type( 'movie', $args );

	}
	
}


