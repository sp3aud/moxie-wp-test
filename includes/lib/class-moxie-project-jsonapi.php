<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/*
	@note original snippet ripped from http://www.deluxeblogtips.com/2010/05/howto-meta-box-wordpress.html
	
	@structure edits
		\_added case logic for number
		\_added default case for failure fallback
		\_minor syntax errors corrected and changes made to match WP coding standards
*/
class Custom_Post_Type_To_Json {

	function cpt_to_json () {

		$cpt_args = array(
			'post_type'    => 'movie'
		);
		$movies = get_posts( $cpt_args );
		//$movies = $cpt_movie->get_posts();

		$cpt_json_movies_json = array ();

		foreach( $movies as $movie ) {
			// This is for the custom fields added to the custom post type 'movie'
			$movie_meta = get_post_meta($movie->ID);

			// This is for the featured image
			$src = wp_get_attachment_image_src( get_post_thumbnail_id($movie->ID), array( 720,405 ), false, '' );

			$cpt_json_movies_json[] = array (
				// Get the ID
				'id' => $movie->ID,
				// Get the title
				'title' => $movie->post_title,
				// Get the Thumbnail URL aka 'poster_url'
				'poster_url' => $src[0],
				// Get the rating
				'rating' => $movie_meta['Rating'][0],
				// Get the year
				'year' => $movie_meta['Year'][0],
				// Get the short_description
				'short_description' => $movie_meta['ShortDescription'][0]
			);
		}
//plugins_url( 'moxie-project/json/movies.json' )
		$path = plugin_dir_path( __FILE__ ). "../../json/movies.json";
		$file = fopen( $path ,"w" );

		if ( file_exists( $path ) ) {
			echo fwrite($file, json_encode($cpt_json_movies_json));
			fclose($file);
		}

	}

}