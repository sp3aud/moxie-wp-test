<?php
/*
 * Plugin Name: Moxie Project
 * Version: 1.0
 * Plugin URI: http://www.joshuamummert.com/
 * Description: Custom plugin - create a JSON API from a custom post type then display JSON data on frontpage (home page of the site).
 * Author: Joshua Mummert
 * Author URI: http://www.joshuamummert.com/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: moxie-project
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Joshua Mummert
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Load plugin class files
require_once( 'includes/class-moxie-project.php' );
require_once( 'includes/class-moxie-project-settings.php' );

// Load plugin libraries
require_once( 'includes/lib/class-moxie-project-admin-api.php' );
require_once( 'includes/lib/class-moxie-project-post-type.php' );
require_once( 'includes/lib/class-moxie-project-metaboxes.php' );
require_once( 'includes/lib/class-moxie-project-jsonapi.php' );
require_once( 'includes/lib/class-moxie-project-procs.php' );
require_once( 'includes/lib/class-moxie-project-taxonomy.php' );

/**
 * Returns the main instance of Moxie_Project to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Moxie_Project
 */
function Moxie_Project () {
	$instance = Moxie_Project::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = Moxie_Project_Settings::instance( $instance );
	}

	return $instance;
}

Moxie_Project();